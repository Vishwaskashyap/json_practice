package com.example.dell.json_practice;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.R.id.progress;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button_headphones = (Button) findViewById(R.id.button_headphones);
        final TextView text_from_web = (TextView)findViewById(R.id.text__from_web);

        button_headphones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Log.i("HEADPHONES","Getting Headphones site Button ");

                UseHttp useHttp = new UseHttp();
                useHttp.execute();

                Log.i("HEADPHONES","Leaving Headphones site Button");

                text_from_web.setText(" [{\"Id\":1,\"Name\":\"Skull Candy\",\"WarehouseName\":\"Mysore\"},{\"Id\":2,\"Name\":\"Sony\",\"WarehouseName\":\"Bangalore\"},{\"Id\":3,\"Name\":\"JBL\",\"WarehouseName\":\"Nanjangud\"},{\"Id\":4,\"Name\":\"Skull Candy 2\",\"WarehouseName\":\"Mysore\"},{\"Id\":5,\"Name\":\"Skull Candy 3\",\"WarehouseName\":\"Mysore\"},{\"Id\":6,\"Name\":\"Skull Candy 4\",\"WarehouseName\":\"Mysore\"},{\"Id\":7,\"Name\":\"Skull Candy 5\",\"WarehouseName\":\"Bangalore\"},{\"Id\":8,\"Name\":\"some name\",\"WarehouseName\":\"ware house name\"},{\"Id\":9,\"Name\":\"some name\",\"WarehouseName\":\"ware house name\"},{\"Id\":10,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":11,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":12,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":13,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":14,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":15,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":16,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":17,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":18,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":19,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":20,\"Name\":\"default name\",\"WarehouseName\":\"ware house name\"},{\"Id\":22,\"Name\":\"android headphones\",\"WarehouseName\":\"android warehouse\"},{\"Id\":23,\"Name\":\"android headphones\",\"WarehouseName\":\"android warehouse\"},{\"Id\":24,\"Name\":\"android headphones\",\"WarehouseName\":\"android warehouse\"},{\"Id\":25,\"Name\":\"android headphones\",\"WarehouseName\":\"android warehouse\"},{\"Id\":26,\"Name\":\"sony\",\"WarehouseName\":\"Bangalore\"},{\"Id\":27,\"Name\":\"Mi4\",\"WarehouseName\":\"Bangalore\"},{\"Id\":28,\"Name\":\"sony\",\"WarehouseName\":\"Bangalore\"},{\"Id\":29,\"Name\":\"sony2\",\"WarehouseName\":\"Nanjangud\"}]\n");
            }
        });
        Button button_chalakas = (Button) findViewById(R.id.button_chalakas);
        final TextView text_from_web1 = (TextView)findViewById(R.id.text__from_web);

        button_chalakas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Log.i("HEADPHONES","Getting Chalakas site Button ");

                UseHttp1 useHttp = new UseHttp1();
                useHttp.execute();

                Log.i("HEADPHONES","Leaving Chalakas site Button");

                text_from_web.setText("<!DOCTYPE html><html lang=\"en\"><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\t<link rel=\"contents\" title=\"Archive\" href=\"/archive\" />\t<link rel=\"start\" title=\"jay's old blog\" href=\"/\" />\t<link type=\"application/rsd+xml\" rel=\"edituri\" title=\"RSD\" href=\"http://blog.thechalakas.com/rsd.axd\" />\t<link type=\"application/rdf+xml\" rel=\"meta\" title=\"SIOC\" href=\"http://blog.thechalakas.com/sioc.axd\" />\t<link type=\"application/apml+xml\" rel=\"meta\" title=\"APML\" href=\"http://blog.thechalakas.com/apml.axd\" />\t<link type=\"application/rdf+xml\" rel=\"meta\" title=\"FOAF\" href=\"http://blog.thechalakas.com/foaf.axd\" />\t<link type=\"application/rss+xml\" rel=\"alternate\" title=\"jay's old blog (RSS)\" href=\"http://blog.thechalakas.com/syndication.axd\" />\t<link type=\"application/atom+xml\" rel=\"alternate\" title=\"jay's old blog (ATOM)\" href=\"http://blog.thechalakas.com/syndication.axd?format=atom\" />\t<link type=\"application/opensearchdescription+xml\" rel=\"search\" title=\"jay's old blog\" href=\"http://blog.thechalakas.com/opensearch.axd\" />\t<link href=\"/Content/Auto/Global.css\" rel=\"stylesheet\" type=\"text/css\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" /><link href=\"themes/Standard/css/bootstrap.min.css\" rel=\"stylesheet\" /><link href=\"themes/Standard/css/main.css\" rel=\"stylesheet\" /><link href=\"themes/Standard/css/responsive.css\" rel=\"stylesheet\" />    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->    <!--[if lt IE 9]>        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>    <![endif]-->    <link rel=\"shortcut icon\" href=\"pics/blogengine.ico\" type=\"image/x-icon\" /><link href=\"editors/tiny_mce_3_5_8/plugins/syntaxhighlighter/styles/shCore.css\" rel=\"stylesheet\" type=\"text/css\" /><link href=\"editors/tiny_mce_3_5_8/plugins/syntaxhighlighter/styles/shThemeDefault.css\" rel=\"stylesheet\" type=\"text/css\" />\t<script type=\"text/javascript\" src=\"/en.res.axd\"></script>\t<script type=\"text/javascript\" src=\"/Scripts/Auto/01-jquery-1.9.1.min.js\"></script>\t<script type=\"text/javascript\" src=\"/Scripts/Auto/02-jquery.cookie.js\"></script>\t<script type=\"text/javascript\" src=\"/Scripts/Auto/04-jquery-jtemplates.js\"></script>\t<script type=\"text/javascript\" src=\"/Scripts/Auto/05-json2.min.js\"></script>\t<script type=\"text/javascript\" src=\"/Scripts/Auto/blog.js\"></script>    <script type=\"text/javascript\" src=\"/editors/tiny_mce_3_5_8/plugins/syntaxhighlighter/scripts/XRegExp.js\"></script>    <script type=\"text/javascript\" src=\"/editors/tiny_mce_3_5_8/plugins/syntaxhighlighter/scripts/shCore.js\"></script>    <script type=\"text/javascript\" src=\"/editors/tiny_mce_3_5_8/plugins/syntaxhighlighter/scripts/shAutoloader.js\"></script>    <script type=\"text/javascript\" src=\"/editors/tiny_mce_3_5_8/plugins/syntaxhighlighter/shActivator.js\"></script>    \t<meta name=\"description\" content=\"this blog will be deleted soon - please visit my new blog - https://thesanguinetechtrainer.com\" />\t<meta name=\"keywords\" content=\"novels,project TD,rants,Technology,tutorials\" />\t<meta name=\"author\" content=\"Jay\" /><title>\tjay's old blog | this blog will be deleted soon - please visit my new blog - https://thesanguinetechtrainer.com</title></head><body class=\"ltr\">    <form method=\"post\" action=\"./\" onsubmit=\"javascript:return WebForm_OnSubmit();\" id=\"aspnetForm\"><div class=\"aspNetHidden\"><input type=\"hidden\" name=\"__EVENTTARGET\" id=\"__EVENTTARGET\" value=\"\" /><input type=\"hidden\" name=\"__EVENTARGUMENT\" id=\"__EVENTARGUMENT\" value=\"\" /><input type=\"hidden\" name=\"__VIEWSTATE\" id=\"__VIEWSTATE\" value=\"O3eyqJMvLFpgBkA+HNR+GOAR8t3ynQ6Bwf9sqqtqSb3I3SaIkYweXbNx6CaJNlgQput7vYEGZ/cZy4jxU0fKHC5jdGu+BqZBteR2lxAVE1zBY6xVxpwGm1W6K8EvH1bdQryNrVOz0QtRyxwtdVt1KWYUNAW\nn");
            }
        });

    }


    class UseHttp extends AsyncTask<String,Void,Void>
{

    protected void onPreExecute()
    {
        Log.i("MainActivity","onPreExecute");
    }

    @Override
    protected Void doInBackground(String... params)
    {
        try
        {

            //final TextView outputView = (TextView) findViewById(R.id.showOutput);
            URL url = new URL("http://simplewebapi1webapp1.azurewebsites.net/api/Headphones");

            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            String urlParameters = "fizz=buzz";
            connection.setRequestMethod("GET");
            connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
            connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");

            int responseCode = connection.getResponseCode();

            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);

            final StringBuilder output = new StringBuilder("Request URL " + url);
            //output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
            output.append(System.getProperty("line.separator")  + "Response Code " + responseCode);
            output.append(System.getProperty("line.separator")  + "Type " + "GET");
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = "";
            StringBuilder responseOutput = new StringBuilder();
            System.out.println("output===============" + br);
            while((line = br.readLine()) != null ) {
                responseOutput.append(line);
            }
            br.close();

            output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run()
                {
                    Log.i("MainActivity","JSON Result - "+output);

                }
            });


        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute()
    {
        Log.i("MainActivity","onPostExecute");
    }

}
    class UseHttp1 extends AsyncTask<String,Void,Void>
    {

        protected void onPreExecute()
        {
            Log.i("MainActivity","onPreExecute");
        }

        @Override
        protected Void doInBackground(String... params)
        {
            try
            {

                //final TextView outputView = (TextView) findViewById(R.id.showOutput);
                URL url1 = new URL("http://blog.thechalakas.com/");

                HttpURLConnection connection = (HttpURLConnection)url1.openConnection();
                String urlParameters = "fizz=buzz";
                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");

                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url1);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url1);
                //output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
                output.append(System.getProperty("line.separator")  + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator")  + "Type " + "GET");
                BufferedReader br1 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br1);
                while((line = br1.readLine()) != null ) {
                    responseOutput.append(line);
                }
                br1.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run()
                    {
                        Log.i("MainActivity","JSON Result - "+output);

                    }
                });


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute()
        {
            Log.i("MainActivity","onPostExecute");
        }

    }

}


